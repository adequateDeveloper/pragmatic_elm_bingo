module Bingo exposing (..)

--    import Http

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput)


--    import Json.Encode as Encode
--    import Json.Decode as Decode exposing (Decoder, field, succeed)
-- MODEL
--    type GameState
--    = EnteringName
--    | Playing


type alias Entry =
    { id : Int
    , phrase : String
    , points : Int
    , marked : Bool
    }


initialEntries : List Entry
initialEntries =
    [ Entry 1 "Future-Proof" 100 False
    , Entry 2 "Doing Agile" 200 False
    , Entry 3 "In The Cloud" 300 False
    , Entry 4 "Rock-Star Ninja" 400 False
    ]


type alias Model =
    { name : String
    , gameNumber : Int
    , entries : List Entry

    -- , alertMessage : Maybe String
    -- , nameInput : String
    -- , gameState : GameState
    }



--    type alias Score =
--        { id : Int
--        , name : String
--        , score : Int
--        }
-- initialModel : Model


initialModel : Model
initialModel =
    { name = "Anonymous"
    , gameNumber = 1
    , entries = initialEntries

    -- , alertMessage = Nothing
    -- , nameInput = ""
    -- , gameState = EnteringName
    }



{-

   {-
      -- no longer used
      initialEntries : List Entry
      initialEntries =
          [ Entry 1 "Future-Proof" 100 False
          , Entry 2 "Doing Agile" 200 False
          , Entry 3 "In The Cloud" 300 False
          , Entry 4 "Rock-Star Ninja" 400 False
          ]
   -}
-}
-- UPDATE


type Msg
    = NewGame



{-

   type Msg
       = NewGame
       | Mark Int
       | SaveName
       | CancelName
       | CloseAlert
       | ShareScore
       | SetNameInput String
       | ChangeGameState GameState
       | NewScore (Result Http.Error Score)
       | NewEntries (Result Http.Error (List Entry))



   --    | NewRandom Int


   update : Msg -> Model -> ( Model, Cmd Msg )
   update msg model =
       case msg of
           {-
              NewRandom randomNumber ->
                  ( { model | gameNumber = randomNumber }, Cmd.none )
           -}
           CloseAlert ->
               ( { model | alertMessage = Nothing }, Cmd.none )

           NewGame ->
               ( { model | gameNumber = model.gameNumber + 1 }, getEntries )

           ShareScore ->
               ( model, postScore model )

           SetNameInput value ->
               ( { model | nameInput = value }, Cmd.none )

           ChangeGameState state ->
               ( { model | gameState = state }, Cmd.none )

           SaveName ->
               ( { model
                   | name = model.nameInput
                   , nameInput = ""
                   , gameState = Playing
                 }
               , Cmd.none
               )

           CancelName ->
               ( { model | nameInput = "", gameState = Playing }, Cmd.none )

           NewScore (Ok score) ->
               let
                   message =
                       "Your score of "
                           ++ (toString score.score)
                           ++ " was sucessfully shared!"
               in
                   ( { model | alertMessage = Just message }, Cmd.none )

           NewScore (Err error) ->
               let
                   message =
                       (++) "Error posting your score: " (toString error)
               in
                   ( { model | alertMessage = Just message }, Cmd.none )

           Mark id ->
               let
                   markEntry anEntry =
                       if anEntry.id == id then
                           { anEntry | marked = (not anEntry.marked) }
                       else
                           anEntry
               in
                   ( { model | entries = List.map markEntry model.entries }, Cmd.none )

           NewEntries (Ok randomEntries) ->
               let
                   _ =
                       Debug.log "Success: " randomEntries
               in
                   ( { model | entries = randomEntries }, Cmd.none )

           NewEntries (Err error) ->
               let
                   _ =
                       Debug.log "Failure: " error

                   errorMessage =
                       case error of
                           Http.NetworkError ->
                               "Is the server running?"

                           Http.BadStatus response ->
                               (toString response.status)

                           Http.BadPayload message _ ->
                               (++) "Decoding Failed: " message

                           _ ->
                               (toString error)
               in
                   ( { model | alertMessage = Just errorMessage }, Cmd.none )



   -- ENCODERS / DECODERS


   encodeScore : Model -> Encode.Value
   encodeScore model =
       Encode.object
           [ ( "name", Encode.string model.name )
           , ( "score", Encode.int (sumMarkedPoints model.entries) )
           ]


   scoreDecoder : Decoder Score
   scoreDecoder =
       Decode.map3 Score
           (field "id" Decode.int)
           (field "name" Decode.string)
           (field "score" Decode.int)


   entryDecoder : Decoder Entry
   entryDecoder =
       Decode.map4 Entry
           (field "id" Decode.int)
           (field "phrase" Decode.string)
           (field "points" Decode.int)
           (succeed False)



   -- COMMANDS
   {-
      -- no longer used
      generateRandomNumber : Cmd Msg
      generateRandomNumber =
          Random.generate NewRandom (Random.int 1 100)
   -}


   entriesUrl : String
   entriesUrl =
       "http://localhost:3000/random-entries"


   getEntries : Cmd Msg
   getEntries =
       (Decode.list entryDecoder)
           |> Http.get entriesUrl
           |> Http.send NewEntries


   postScore : Model -> Cmd Msg
   postScore model =
       let
           url =
               "http://localhost:3000/scores"

           body =
               encodeScore model
                   |> Http.jsonBody

           request =
               Http.post url body scoreDecoder
       in
           Http.send NewScore request



   -- VIEW

   viewAlertMessage : Maybe String -> Html Msg
   viewAlertMessage alertMessage =
       case alertMessage of
           Just message ->
               div [ class "alert" ]
                   [ span [ class "close", onClick CloseAlert ] [ text "X" ]
                   , text message
                   ]

           Nothing ->
               text ""


   viewNameInput : Model -> Html Msg
   viewNameInput model =
       case model.gameState of
           EnteringName ->
               div [ class "name-input" ]
                   [ input
                       [ type_ "text"
                       , placeholder "Who's playing"
                       , autofocus True
                       , value model.nameInput
                       , onInput SetNameInput
                       ]
                       []
                   , button [ onClick SaveName ] [ text "Save" ]
                   , button [ onClick CancelName ] [ text "Cancel" ]
                   ]

           Playing ->
               text ""

-}


playerInfo : String -> Int -> String
playerInfo name gameNumber =
    name ++ " - Game #" ++ (toString gameNumber)


viewPlayer : String -> Int -> Html msg
viewPlayer name gameNumber =
    let
        playerInfoText =
            playerInfo name gameNumber
                |> String.toUpper
                |> text
    in
        h2 [ id "info", class "classy" ] [ playerInfoText ]



{-
   h2 [ id "info", class "classy" ]
       [ a [ href "#", onClick (ChangeGameState EnteringName) ]
           [ text name ]
       , text (" - Game #" ++ (toString gameNumber))
       ]
-}


viewHeader : String -> Html msg
viewHeader title =
    header [] [ h1 [] [ text title ] ]


viewFooter : Html msg
viewFooter =
    footer []
        [ a [ href "http://elm-lang.org" ] [ text "Powered by Elm" ] ]


viewEntryItem : Entry -> Html msg
viewEntryItem entry =
    li []
        -- li [ classList [ ( "marked", entry.marked ) ], onClick <| Mark entry.id ]
        [ span [ class "phrase" ] [ text entry.phrase ]
        , span [ class "points" ] [ text <| toString entry.points ]
        ]


viewEntryList : List Entry -> Html msg
viewEntryList entries =
    entries
        |> List.map viewEntryItem
        |> ul []



{-

   viewScore : Int -> Html Msg
   viewScore sum =
       div
           [ class "score" ]
           [ span [ class "label" ] [ text "Score" ]
           , span [ class "value" ] [ text <| toString sum ]
           ]


   sumMarkedPoints : List Entry -> Int
   sumMarkedPoints entries =
       entries
           |> List.filter .marked
           |> List.map .points
           |> List.sum

-}


view : Model -> Html Msg
view model =
    div [ class "content" ]
        [ viewHeader "BUZZWORD BINGO"
        , viewPlayer model.name model.gameNumber

        --    , viewAlertMessage model.alertMessage
        --    , viewNameInput model
        , viewEntryList model.entries

        --    , viewScore (sumMarkedPoints model.entries)
        , div [ class "button-group" ] [ button [ onClick NewGame ] [ text "New Game" ] ]

        --        , button [ onClick ShareScore ] [ text "Share Score" ]
        --        ]
        , div [ class "debug" ] [ text <| toString model ]
        , viewFooter
        ]



{-
   main : Program Never Model Msg
   main =
       program
           { init = ( initialModel, getEntries )
           , view = view
           , update = update
           , subscriptions = (always Sub.none)
           }
-}


main : Html Msg
main =
    view initialModel
