{-
   From Ch17 Notes:

   It's often helpful to have a standalone Elm file in which to play around with decoding different JSON strings,
   without needing a backend API.

   Then to try other examples, you just need to change the values of json and decoder. Notice that the json value
   uses """ to mark the beginning and ending of a multi-line string which is quite handy in this situation.
-}


module DecodingPlayground exposing (..)

import Html exposing (..)
import Json.Decode exposing (..)


json =
    """
    {
      "id": 1,
      "phrase": "Future-Proof",
      "points": 100
    }
    """


decoder =
    field "id" int


decodingResult =
    decodeString decoder json


view result =
    case result of
        Ok value ->
            text (toString value)

        Err err ->
            text (toString err)


main =
    view decodingResult
